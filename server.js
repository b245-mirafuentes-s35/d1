const express = require("express")

// mongoose is a package that allows us to create schemas to model our data structure to manipulate our data base using different access methods.
const mongoose = require('mongoose')

const port = 3001

const app = express()

// app.express.json()
	
	//[Section] MongoDB connection
		//Syntax:
			// mongoose.connect("mongoDBconnectionString", {option to avoid errors in our connection})
	
	mongoose.connect("mongodb+srv://admin:admin@batch245-mirafuentes.m5uejs3.mongodb.net/s35-discussion?retryWrites=true&w=majority",{
		// Allows us to avoid any current and future errors while connecting to MONGODB
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	let db = mongoose.connection;

	// error handling in connecting
	db.on("error", console.error.bind(console, "Connection error"))

	// this will be triggered if the connection is successful
	db.once("open", () => console.log("we're connected to the cloud database"))

		// mongoose Schemas
			// Schemas determine the structure of the documents to be written in the database
			// Schemas act as the blueprint to our data
			// Syntax:
				// const schemaName = new mongoose.Schema({keyvaluepairs})

		// taskSchema it contains two properties: name & status
		// required is used to specify that a field must not be empty
		// default - is used if a field value is not supplied
		const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, "Task name is required"]
			},
			status: {
				type: String,
				default: "pending"
			}
		})

		// [Section] Models
			// Uses schema to create/instatiate documents/objects that follows our schema structure
			// the variable/object that will be created can be used to run commands with our database
			// Syntax:
				// const variableName = mongoose.model("collectionName", schemaName)

		const Task = mongoose.model("Task", taskSchema)

	//midlewares
	app.use(express.json()) // allows the app to read json
	app.use(express.urlencoded({extended: true})) // it will allow our app to read data from forms

	// [Section] Routing
		// create/add new task
			// 1. Check if the task is existing.
					// if task already exist in the database, we will return a message "the task is already existing!"
					// if the task doesnt exist in the database, we will add it in the database
	app.post("/tasks", (request, response) => {
		let input = request.body

			console.log(input.status)
			if(input.status === undefined){
				Task.findOne({name: input.name}, (error, result) => {
					console.log(result)
					if(result !== null){
						return response.send("the task is already existing")
					}else{
						let newTask = new Task({
							name: input.name
						})
						// save() method wil save the object in the collection that the object instatiated
						newTask.save((saveError, savedTask) => {
							if(saveError){
								return console.log(saveError)
							}else{
								return response.send("New task Created")
							}
						})
					}
				})
			}else{
				Task.findOne({name: input.name}, (error, result) => {
					console.log(result)
					if(result !== null){
						return response.send("the task is already existing")
					}else{
						let newTask = new Task({
							name: input.name,
							status: input.status
						})
						// save() method wil save the object in the collection that the object instatiated
						newTask.save((saveError, savedTask) => {
							if(saveError){
								return console.log(saveError)
							}else{
								return response.send("New task Created")
							}
						})
					}
				})
			}
	})

	// [Section] Retrieving the all tasks

	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error){
				return console.log(error)
			}else{
				return response.send(result)
			}
		})
	})

	// ACTIVITY
	const userSchema = new mongoose.Schema({
		user: {
			type: String,
			required: [true, "User is required"]
		},
		password: {
			type: String,
		}
	})

	const User = mongoose.model("User", userSchema)

	app.post("/signup", (request, response) => {
		let input = request.body
				User.findOne({user: input.user},  (error, result) => {
					console.log(result)
					if(result !== null){
						return response.send("the user is already existing")
					}else{
						let newUser = new User({
							user: input.user,
							password: input.password
						})
						newUser.save((saveError, savedTask) => {
							if(saveError){
								return console.log(saveError)
							}else{
								return response.send("New user registered")
							}
						})
				}
		})
	}) 
app.listen(port, console.log(`Server is running at port ${port}`)) 